import pandas as pd
from pandas import DataFrame
import numpy as np
import json
import csv
import os
import re
import sys
import statistics as stats

# Directorio actual
path = os.getcwd()
path_json = path + '/../Data/json/'
path_csv = path + '/../Data/csv/'

# Ficheros de datos
# ************************************************************************************************************
data_json_run8 = path_json + "run8.json"
data_csv_run8 = path_csv + "run8_labeled.csv"
# ************************************************************************************************************
data_json_run11 = path_json + "run11.json"
data_csv_run11 = path_csv + "run11_labeled.csv"
# ************************************************************************************************************
data_json_run1_6 = path_json + "run1_6rtu.json"
data_csv_run1_6 = path_csv + "run1_6rtu_labeled.csv"
# ************************************************************************************************************
data_json_run1_12 = path_json + "run1_12rtu.json"
data_csv_run1_12 = path_csv + "run1_12rtu_labeled.csv"
# ************************************************************************************************************
data_json_run1_3 = path_json + "run1_3rtu_2s.json"
data_csv_run1_3 = path_csv + "run1_3rtu_2s_labeled.csv"
# ************************************************************************************************************
data_json_polling = path_json + "Modbus_polling_only_6RTU.json"
data_csv_polling = path_csv + "Modbus_polling_only_6RTU_labeled.csv"
# ************************************************************************************************************
data_json_fake = path_json + "send_a_fake_command_modbus_6RTU_with_operate.json"
data_csv_fake = path_csv + "send_a_fake_command_modbus_6RTU_with_operate_labeled.csv"
# ************************************************************************************************************
data_json_characterization = path_json + "characterization_modbus_6RTU_with_operate.json"
data_csv_characterization = path_csv + "characterization_modbus_6RTU_with_operate_labeled.csv" 
# ************************************************************************************************************
data_json_uploading = path_json + "CnC_uploading_exe_modbus_6RTU_with_operate.json"
data_csv_uploading = path_csv + "CnC_uploading_exe_modbus_6RTU_with_operate_labeled.csv"
# ************************************************************************************************************
data_json_ms08_netapi = path_json + "exploit_ms08_netapi_modbus_6RTU_with_operate.json"
data_csv_ms08_netapi = path_csv + "exploit_ms08_netapi_modbus_6RTU_with_operate_labeled.csv"
# ************************************************************************************************************
data_json_moving = path_json + "moving_two_files_modbus_6rtu.json"
data_csv_moving = path_csv + "moving_two_files_modbus_6RTU_labeled.csv"
# ************************************************************************************************************

# Lista de tuplas con los ficheros (json, csv)
files = [
    (data_json_run8, data_csv_run8),
    (data_json_run11, data_csv_run11),
    (data_json_run1_6, data_csv_run1_6),
    (data_json_run1_12, data_csv_run1_12),
    (data_json_run1_3, data_csv_run1_3),
    (data_json_polling, data_csv_polling),
    (data_json_uploading, data_csv_uploading),
    (data_json_fake, data_csv_fake),
    #(data_json_characterization, data_csv_characterization),   # Este fichero lo eliminamos de las capturas por las inconsistencias explicadas en la memoria
    (data_json_ms08_netapi, data_csv_ms08_netapi),
    (data_json_moving, data_csv_moving)
        ]

# Nombre del fichero donde se van a imprimir las estadísticas
file_stats = 'stats.txt'

# Eliminamos el fichero de estadisticas si existe previamente
if os.path.exists(file_stats):
    os.remove(file_stats)

# Caracteristicas usadas para la primera aproximación
header = [
    'frame.time',
    'frame.len', 
    'ip.proto',
    'ip.len',
    'ip.src',
    'ip.dst',
    'ip.srcport',
    'ip.dstport',
    'tcp.len',
    'udp.length',
    # modbus
    'mbtcp.len', 
    'mbtcp.trans_id', 
    'modbus.func_code', 
    'modbus.reference_num', 
    'modbus.bit_cnt',
    'modbus.word_cnt',
    'modbus.num_reg', # Made by us (indlude: 'modbus.bitnum', 'modbus.bitval'etc.)
    'target'
]

# Función que cuenta el número de registros que aparecen en una respuesta Modbus
def registerCount(dicc):
    expresion = 'Register\s\d|Bit\s\d'
    count = 0
    for key, value in dicc.items():
        # Comprobamos si para par clave-valor, la clave machea con la expresión regular
        if re.match(expresion, key):
            count += 1
    return count

# Función recursiva que recorre un diccionario y todos los posibles subdiccionarios dentro de este
def rec(dictionary, aux):
    for key, value in dictionary.items():
        # Si el par clave-valor son strings los añadimos al diccionario.
        if (key.__class__ is str and value.__class__ is str):
            aux[key] = value
            # Si es un puerto tcp o udp origen lo mapeamos a ip.srcport
            if ((key == 'tcp.srcport') | ((key == 'udp.srcport'))):
                aux['ip.srcport'] = value
            # Si es un puerto tcp o udp destino lo mapeamos a ip.dstport
            if ((key == 'tcp.dstport') | ((key == 'udp.dstport'))):
                aux['ip.dstport'] = value
        # Si el valor de la clave es de tipo dict, hacemos una llamada recursiva a la funcion.
        if value.__class__ is dict:
            rec(value, aux)
            # Funcion auxiliar que cuenta el numero de registros que aparecen en una respuesta Modbus
            if (key == "modbus"):
                count = registerCount(value)
                if (count != 0):
                    aux["modbus.num_reg"] = str(count)
    return aux

# Fución que transforma un json a un DataFrame. Cada linea del DataFrame se corresponde con un paquete del json
def toDataFrame(data):
    dicc = dict()
    key = 0
    # Para cada paquete en el fichero json, hacemos la llamada a la función recursiva sobre el valor de la clave '_source'
    for line in data:
        aux = {}
        packet = line['_source']
        aux = rec(packet,aux)
        dicc[key] = aux
        key += 1
    df = pd.DataFrame(dicc)
    df = df.transpose()
    return df

# Dataframe utilizado para almacenar las estadísticas
complet_dataframe = pd.DataFrame()

# Concatenamos todos los dataframes parciales en uno final
for file in files:
    # Fichero JSON
    f_json = file[0]
    # Fichero CSV
    f_csv = file[1]
    data_json = json.load(open(f_json))
    df_tmp_json = toDataFrame(data_json)   
    df_tmp_csv = pd.read_csv(f_csv, sep = ';', names = ['index', 'target'])
    df_tmp_json['target'] = df_tmp_csv['target']
    complet_dataframe = pd.concat([complet_dataframe, df_tmp_json])

# Nos quedamos con las carateristicas que nos interesan
complet_dataframe = complet_dataframe[header]

# Dataframe solo con las filas pertenecientes a la categoria ataque
ip = complet_dataframe[complet_dataframe['target'] == 1]

# Estadisticas Generales
def generalStats(data):
    normalTCP = len(data[(data['ip.proto'] == "6") & (data['target'] == 0)])
    atackTCP = len(data[(data['ip.proto']  == "6") & (data['target'] == 1)])
    normalUDP = len(data[(data['ip.proto']  == "17") & (data['target'] == 0)])
    atackUDP = len(data[(data['ip.proto']  == "17") & (data['target'] == 1)])
    with open(file_stats, 'a+') as f:
        f.write('[*] Estadisticas generales\n')
        f.write('[*] Numero de paquetes TCP normales: ' + str(normalTCP) + '\n')
        f.write('[*] Numero de paquetes TCP ataque: '  + str(atackTCP) + '\n')
        f.write('[*] Numero de paquetes UDP normales: ' + str(normalUDP) + '\n')
        f.write('[*] Numero de paquetes UDP ataque: ' + str(atackUDP) + '\n')
        f.write('[*] Numero de paquetes totales que aparecen en la captura: ' + str(len(complet_dataframe)) + '\n')
        f.write('[*] Numero de bytes que ocupa el Dataframe en memeria: ' + str(sys.getsizeof(complet_dataframe)) + '\n')
        f.write('[*] Dimensiones del dataframe' + str(complet_dataframe.shape) + '\n')
        f.write('[*] Tamaño del Dataframe: ' + str(complet_dataframe.size) + '\n')
        f.write('\n')

generalStats(complet_dataframe)

# Analisis longitud de paquetes
def length_stats(data):
    df_tcp = data[(data['ip.proto'] == "6") & (data['target'] == 0)]
    df_udp = data[(data['ip.proto'] == "17") & (data['target'] == 0)]
    df_tcp_attack = data[(data['ip.proto'] == "6") & (data['target'] == 1)]
    df_udp_attack = data[(data['ip.proto'] == "17") & (data['target'] == 1)]
    df_global = data[(data['ip.proto'] == "17") | (data['ip.proto'] == "6")]
    with open(file_stats, 'a+') as f:
        f.write('[*] Máximo, mínimo, media y mediana\n')
        f.write('[*] -----------------------------' + '\n')
        f.write('[*] MAX TCP: ' + str(max(df_tcp['ip.len'].astype(float).values)) + '\n')
        f.write('[*] MIN TCP: ' + str(min(df_tcp['ip.len'].astype(float).values)) + '\n')
        f.write('[*] MEDIAN TCP: ' + str(stats.median(df_tcp['ip.len'].astype(float).values)) + '\n')
        f.write('[*] MEAN TCP: ' + str(stats.mean(df_tcp['ip.len'].astype(float).values)) + '\n')
        f.write('[*] -----------------------------' + '\n')
        f.write('[*] MAX UDP: ' + str(max(df_udp['ip.len'].astype(float).values)) + '\n')
        f.write('[*] MIN UDP: ' + str(min(df_udp['ip.len'].astype(float).values)) + '\n')
        f.write('[*] MEDIAN UDP: ' + str(stats.median(df_udp['ip.len'].astype(float).values)) + '\n')
        f.write('[*] MEAN UDP: ' + str(stats.mean(df_udp['ip.len'].astype(float).values)) + '\n')
        f.write('[*] -----------------------------' + '\n')
        f.write('[*] MAX TCP ATTACK: ' + str(max(df_tcp_attack['ip.len'].astype(float).values)) + '\n')
        f.write('[*] MIN TCP ATTACK: ' + str(min(df_tcp_attack['ip.len'].astype(float).values)) + '\n')
        f.write('[*] MEDIAN TCP ATTACK: ' + str(stats.median(df_tcp_attack['ip.len'].astype(float).values)) + '\n')
        f.write('[*] MEAN TCP ATTACK: ' + str(stats.mean(df_tcp_attack['ip.len'].astype(float).values)) + '\n')
        f.write('[*] -----------------------------' + '\n')
        f.write('[*] MAX UDP ATTACK: ' + str(max(df_udp_attack['ip.len'].astype(float).values)) + '\n')
        f.write('[*] MIN UDP ATTACK: ' + str(min(df_udp_attack['ip.len'].astype(float).values)) + '\n')
        f.write('[*] MEDIAN UDP ATTACK: ' + str(stats.median(df_udp_attack['ip.len'].astype(float).values)) + '\n')
        f.write('[*] MEAN UDP ATTACK: ' + str(stats.mean(df_udp_attack['ip.len'].astype(float).values)) + '\n')
        f.write('[*] -----------------------------' + '\n')
        f.write('[*] MAX GLOBAL: ' + str(max(df_global['ip.len'].astype(float).values)) + '\n')
        f.write('[*] MIN GLOBAL: ' + str(min(df_global['ip.len'].astype(float).values)) + '\n')
        f.write('[*] MEDIAN GLOBAL: ' + str(stats.median(df_global['ip.len'].astype(float).values)) + '\n')
        f.write('[*] MEAN GLOBAL: ' + str(stats.mean(df_global['ip.len'].astype(float).values)) + '\n')
        f.write('\n')

length_stats(complet_dataframe)

# Analisis IP
def ip_stats(ip, exp):
    # Expresiones regulares que machean con cada clase de IP
    # 192.168.1.99-100
    mtu = '192\.168\.1\.99|192\.168\.1\.100'
    # 192.168.1.101-115
    rtu = '192\.168\.1\.10[1-9]|192\.168\.1\.11[0-5]'
    # 10. | 172.16-31. | 192.168. | 224.
    desconocida = '10\.*|172\.1[6-9]\..*|172\.2[0-9]\..*|172\.3[0-1]\..*|192\.168\.\d{1,3}\.\d{1,3}|224\..*'
    # Contadores para cada clase
    count_mtu = 0
    count_rtu = 0
    count_desconocida = 0
    count_wan = 0
    # Creamos un objeto iterable a partir de la serie que le pasamos a la función
    iterable = ip.items()
    # Iteramos sobre el objeto
    for i in iterable:
        # La IP se corresponde con una MTU
        if re.match(mtu, i[0]):
            count_mtu += i[1]
        # La IP se corresponde con una RTU
        elif re.match(rtu, i[0]):
            count_rtu += i[1]
        # La IP se corresponde con una IP desconocida (IP privadas pertenecientes a otra red, multicast...)
        elif re.match(desconocida, i[0]):
            count_desconocida += i[1]
        # Si no es ninguna de las anteriores la clasificamos como una WAN
        else:
            count_wan += i[1]
    # Imprimimos las estadisticas
    with open(file_stats, 'a+') as f:
        f.write('[*] IP ' + exp + '\n')
        f.write('[*] IPs correspondientes a MTUs: ' + str(count_mtu) + '\n')
        f.write('[*] IPs correspondientes a RTUs: ' + str(count_rtu)+ '\n')
        f.write('[*] IPs desconocidas: ' + str(count_desconocida)+ '\n')
        f.write('[*] IPs correspondientes a WANs: ' + str(count_wan)+ '\n')
        f.write('[*] -----------------------------------------'+ '\n')
        f.write('[*] TOTAL de IPs macheadas: ' + str(count_mtu + count_rtu + count_desconocida + count_wan)+ '\n')
        f.write('[*] TOTAL de IPs: ' + str(sum(ip.values))+ '\n')
        f.write('\n')

# Obtenemos el número de valores
ip_src = complet_dataframe['ip.src'].value_counts()
ip_dst = complet_dataframe['ip.dst'].value_counts()
ip_src_attack = ip['ip.src'].value_counts()
ip_dst_attack = ip['ip.src'].value_counts()

# Hacemos uso de la función para ip_src
ip_stats(ip_src, 'Origen Normal')

# Hacemos uso de la función para ip_dst
ip_stats(ip_dst, 'Destino Normal')

# Hacemos uso de la función para ip_src
ip_stats(ip_src_attack, 'Origen Ataque')

# Hacemos uso de la función para ip_dst
ip_stats(ip_dst_attack, 'Destino Ataque')

# Analisis de puertos
def port_stats(port, exp):
    # Contadores para cada tipo de puerto
    conocidos = 0
    registrados = 0
    privados = 0
    # Creamos un objeto iterable a partir de la serie que le pasamos a la función
    iterable = port.items()
    for i in iterable:
        # Si el puerto es conocido [1-1023]
        if (int(i[0]) > 1) & (int(i[0]) < 1024):
            conocidos += i[1]
        # Si el puerto es registrado [1024-49151]
        elif (int(i[0]) > 1023) & (int(i[0]) < 49151):
            registrados += i[1]
        # Si el puerto es privado > 49151
        else:
            privados += i[1]
    # Imprimimos las estadisticas
    with open(file_stats, 'a+') as f:
        f.write('[*] Puerto ' + exp + '\n')
        f.write('[*] Puertos conocidos: ' + str(conocidos) + '\n')
        f.write('[*] Puertos registrados: ' + str(registrados) + '\n')
        f.write('[*] Puertos privados: ' + str(privados) + '\n')
        f.write('[*] -----------------------------------------' + '\n')
        f.write('[*] TOTAL de Puertos macheados: ' + str(conocidos + registrados + privados) + '\n')
        f.write('[*] TOTAL de Puertos: ' + str(sum(port.values)) + '\n')
        f.write('\n')

# Obtenemos el número de valores
src_port = complet_dataframe['ip.srcport'].value_counts()
dst_port = complet_dataframe['ip.dstport'].value_counts()
port_src_attack = ip['ip.srcport'].value_counts()
port_dst_attack = ip['ip.dstport'].value_counts()

# Hacemos uso de la función para port_src
port_stats(src_port, 'Origen Normal')

# Hacemos uso de la función para port_dst
port_stats(dst_port, 'Destino Normal')

# Hacemos uso de la función para port_src
port_stats(port_src_attack, 'Origen Ataque')

# Hacemos uso de la función para port_dst
port_stats(port_dst_attack, 'Destino Ataque')

# Para cada característica sacamos el número de valores
with open(file_stats, 'a+') as f:
    f.write('[*] Numero de valores \n')
    for col in complet_dataframe.columns.values:
        f.write(str(col)  + ' ' + str(complet_dataframe[col].count()) +'\n')


# Para cada característica sacamos el número de valores distintos
with open(file_stats, 'a+') as f:
    f.write('[*] Numero de valores distintos \n')
    for col in complet_dataframe.columns.values:
        f.write(str(col) + ' ' + str(len(complet_dataframe[col].value_counts())) + '\n')


