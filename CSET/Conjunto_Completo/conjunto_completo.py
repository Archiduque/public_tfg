import pandas as pd
from pandas import DataFrame
import numpy as np
import json
import csv
import os
import re
from datetime import datetime
import statistics as stats

# Directorio actual
path = os.getcwd()
path_json = path + '/Data/json/'
path_csv = path + '/Data/csv/'

# Ficheros de datos
# ************************************************************************************************************
data_json_run8 = path_json + "run8.json"
data_csv_run8 = path_csv + "run8_labeled.csv"
# ************************************************************************************************************
data_json_run11 = path_json + "run11.json"
data_csv_run11 = path_csv + "run11_labeled.csv"
# ************************************************************************************************************
data_json_run1_6 = path_json + "run1_6rtu.json"
data_csv_run1_6 = path_csv + "run1_6rtu_labeled.csv"
# ************************************************************************************************************
data_json_run1_12 = path_json + "run1_12rtu.json"
data_csv_run1_12 = path_csv + "run1_12rtu_labeled.csv"
# ************************************************************************************************************
data_json_run1_3 = path_json + "run1_3rtu_2s.json"
data_csv_run1_3 = path_csv + "run1_3rtu_2s_labeled.csv"
# ************************************************************************************************************
data_json_polling = path_json + "Modbus_polling_only_6RTU.json"
data_csv_polling = path_csv + "Modbus_polling_only_6RTU_labeled.csv"
# ************************************************************************************************************
data_json_fake = path_json + "send_a_fake_command_modbus_6RTU_with_operate.json"
data_csv_fake = path_csv + "send_a_fake_command_modbus_6RTU_with_operate_labeled.csv"
# ************************************************************************************************************
data_json_characterization = path_json + "characterization_modbus_6RTU_with_operate.json"
data_csv_characterization = path_csv + "characterization_modbus_6RTU_with_operate_labeled.csv" 
# ************************************************************************************************************
data_json_uploading = path_json + "CnC_uploading_exe_modbus_6RTU_with_operate.json"
data_csv_uploading = path_csv + "CnC_uploading_exe_modbus_6RTU_with_operate_labeled.csv"
# ************************************************************************************************************
data_json_ms08_netapi = path_json + "exploit_ms08_netapi_modbus_6RTU_with_operate.json"
data_csv_ms08_netapi = path_csv + "exploit_ms08_netapi_modbus_6RTU_with_operate_labeled.csv"
# ************************************************************************************************************
data_json_moving = path_json + "moving_two_files_modbus_6rtu.json"
data_csv_moving = path_csv + "moving_two_files_modbus_6RTU_labeled.csv"
# ************************************************************************************************************

# Lista de tuplas con los ficheros (json, csv)
files = [
    (data_json_run8, data_csv_run8),
    (data_json_run11, data_csv_run11),
    (data_json_run1_6, data_csv_run1_6),
    (data_json_run1_12, data_csv_run1_12),
    (data_json_run1_3, data_csv_run1_3),
    (data_json_polling, data_csv_polling),
    (data_json_uploading, data_csv_uploading),
    (data_json_fake, data_csv_fake),
    #(data_json_characterization, data_csv_characterization),
    (data_json_ms08_netapi, data_csv_ms08_netapi),
    (data_json_moving, data_csv_moving)
        ]

# Variables que representan a los ficheros que vamos a utilizar
identifiers = "identifiers.txt"
targets = "targets.txt"
numerical = "numericals.txt"
categorical = "categoricals.txt"
dictionary = "dictionary.json"

# Fichero auxiliar
tmp_file = "aux.txt"

# Lista con los ficheros necesarios para la SOM
list_files_required = [ 
    identifiers,
    targets,
    numerical,
    categorical,
    dictionary,
    tmp_file
    ]

# Si existen los ficheros, los eliminamos
for f in list_files_required:
    if os.path.exists(f):
        os.remove(f)

# Caracteristicas usadas para la primera aproximación
header = [
    'frame.time',
    'frame.len', 
    'ip.proto',
    'ip.len',
    'ip.src',
    'ip.dst',
    'ip.srcport',
    'ip.dstport',
    'tcp.len',
    'udp.length',
    # modbus
    'mbtcp.len', 
    'mbtcp.trans_id', 
    'modbus.func_code', 
    'modbus.reference_num', 
    'modbus.bit_cnt',
    'modbus.word_cnt',
    'modbus.num_reg', # Made by us (indlude: 'modbus.bitnum', 'modbus.bitval'etc.)
    #'modbus.data',
    'target'
]

# Función que cuenta el número de registros que aparecen en una respuesta Modbus
def registerCount(dicc):
    expresion = 'Register\s\d|Bit\s\d'
    count = 0
    for key, value in dicc.items():
        # Comprobamos si para par clave-valor, la clave machea con la expresión regular
        if re.match(expresion, key):
            count += 1
    return count

# Función recursiva que recorre un diccionario y todos los posibles subdiccionarios dentro de este
def rec(dictionary, aux):
    for key, value in dictionary.items():
        # Si el par clave-valor son strings los añadimos al diccionario.
        if (key.__class__ is str and value.__class__ is str):
            aux[key] = value
            # Si es un puerto tcp o udp origen lo mapeamos a ip.srcport
            if ((key == 'tcp.srcport') | ((key == 'udp.srcport'))):
                aux['ip.srcport'] = value
            # Si es un puerto tcp o udp destino lo mapeamos a ip.dstport
            if ((key == 'tcp.dstport') | ((key == 'udp.dstport'))):
                aux['ip.dstport'] = value
        # Si el valor de la clave es de tipo dict, hacemos una llamada recursiva a la funcion.
        if value.__class__ is dict:
            rec(value, aux)
            # Funcion auxiliar que cuenta el numero de registros que aparecen en una respuesta Modbus
            if (key == "modbus"):
                count = registerCount(value)
                if (count != 0):
                    aux["modbus.num_reg"] = str(count)
    return aux

# Fución que transforma un json a un DataFrame. Cada linea del DataFrame se corresponde con un paquete del json
def toDataFrame(data):
    dicc = dict()
    key = 0
    # Para cada paquete en el fichero json, hacemos la llamada a la función recursiva sobre el valor de la clave '_source'
    for line in data:
        aux = {}
        packet = line['_source']
        aux = rec(packet,aux)
        dicc[key] = aux
        key += 1
    df = pd.DataFrame(dicc)
    df = df.transpose()
    return df

# Dataframe utilizado para almacenar las estadísticas
complet_dataframe = pd.DataFrame()

# Concatenamos todos los dataframes parciales en uno final
for file in files:
    # Fichero JSON
    f_json = file[0]
    # Fichero CSV
    f_csv = file[1]
    data_json = json.load(open(f_json))
    df_tmp_json = toDataFrame(data_json)   
    df_tmp_csv = pd.read_csv(f_csv, sep = ';', names = ['index', 'target'])
    df_tmp_json['target'] = df_tmp_csv['target']
    complet_dataframe = pd.concat([complet_dataframe, df_tmp_json])

complet_dataframe = complet_dataframe[header]

# Funcion para parsear la columna frame.time
def date_parser(date):
    formato = "%b %d, %Y %H:%M:%S.%f %Z"
    object_datetime = datetime.strptime(date, formato)
    return object_datetime

# Parseo de la columna frame.time
complet_dataframe['frame.time'] = complet_dataframe['frame.time'].map(lambda x : x.replace('000 ', ' ')).map(lambda x : date_parser(x))


# Lista con las caracteristicas numericas
numerical_headers = [
    #'frame.time',    # La característica tiempo no modificarla
    'frame.len', 
    'ip.len',
    'tcp.len',
    'udp.length',
    'mbtcp.len',
    'modbus.reference_num', 
    'modbus.bit_cnt',
    'modbus.word_cnt',
    'modbus.num_reg', # Made by us (indlude: 'modbus.bitnum', 'modbus.bitval'etc.)
    ]

# Lista con las caracteristicas categoricas
categorical_headers = [
    'ip.proto',
    'ip.src',
    'ip.dst',
    'ip.srcport',
    'ip.dstport',
    'mbtcp.trans_id',   # Mirar este
    'modbus.func_code',
    ]

# Diccionario con los pares 'Paquete:Etiqueta'
attacks = { 
    0: 'Non_Attack',
    1: 'Attack',
}

# En primer lugar, tenemos que generar un fichero de índices que contenga el mismo número de entradas
# que el dataframe. Para ello creamos la siguiente función:

def generateIdentifiers(length, filename):
    
    file = open(filename, "w")
    for line in range(0, length):
        file.write(str(line) + "\n")
    file.close()

# **************************************************************    
# Descomentar la siguiente linea para generar fichero de indices
generateIdentifiers(len(complet_dataframe), identifiers)

# Targets
# 
targets_df = complet_dataframe['target']

# Mapeamos los códigos de los ataques con los nombres de los mismos
target = targets_df.map(lambda x: attacks[x])

# **************************************************************
# Descomentar la siguiente linea para generar fichero targets
target.to_csv(targets, header = False, index = False, sep = ' ')




# Numericals

# Antes de operar debemos transformar los datos, eliminando los NaN en este caso por la media de cada caracteristica.
def parser(data):
    # Recorremos el Dataframe por columnas (Series)
    for header in data:
        series = data[header]
        list_mean = []
        # Creamos una lista con todos los elementos para posteriormente calcular la media
        for element in series:
            if str(element) != str(np.NaN):
                list_mean.append(float(element))
        mean = stats.mean(list_mean)
        # Recorremos la serie substituyendo el valor NaN por media
        new_serie = series.map(lambda x : mean if str(x) == str(np.NaN) else float(x))
        data[header] = new_serie
    return data
    
# Los datos numericos los normalizamos de la forma: (x - min)/(max-min)
def normalized(data):
    # Recorremos el Dataframe por columnas (Series)
    for header in data:
        series = data[header]
        maximum = max(series)
        minimum = min(series)
        divider = maximum - minimum
        if (divider != 0):
            # Aplicamos la funcion a cada elemento de la serie
            new_serie = series.map( lambda x : ((x - minimum) / (divider)))
        else:
            new_serie = series.map( lambda x : 1)
        data[header] = new_serie
    return data

# Creamos un nuevo dataframe con las caracteristicas de tipo numerico
df_num = complet_dataframe[numerical_headers]

# Convertimos las columnas de tipo Object a Float
df_num.dtypes
df_num = df_num.astype(float)
    
# Transaformamos los datos
aux_df = parser(df_num)

# Posteriormente los normalizamos
numerical_df = normalized(aux_df)

# Añadir la columna fecha al dataframe 
numerical_df['frame.time'] = complet_dataframe['frame.time']

# **************************************************************
# Funcion que imprime las cabeceras correctamente
def writeHeader(file, data):
    # Creamos el fichero con los datos numericos y las cabeceras
    data.to_csv(tmp_file, header = True, index = False, sep = ' ')
    # Abrimos los ficheros
    aux_file = open(tmp_file)
    file = open(file, "w")
    # Añadimos '#' al principio de las cabeceras
    file.write('# ')
    for line in aux_file:
        file.write(line)
    # Cerramos los ficheros
    aux_file.close()
    file.close()
    # Eliminamos el fichero temporal
    os.remove(tmp_file)
    
# Descomentar la siguiente linea para generar fichero numericals
writeHeader(numerical, numerical_df)







# Categoricals

def toDict(val, dic):
    keys = dic.keys()
    for k in keys:
        get = dic.get(k)
        if get == val:
            return k

# Codificamos los valores corresponfientes a cada columna a la forma 0..N, siendo N el numero máximo de elementos
def codify(data, headers):
    dictionary = dict()
    # Recorremos el Dataframe por columnas (Series)
    for header in data:
        series = data[header]
        # Obtenemos un objeto con los valores distintos que hay en la serie y el numero de veces que este aparece
        tmp = series.value_counts()
        index = []
        # Añadimos todos los valores distintos de la serie a una lista iterando la parte index del objeto anterior
        for value in tmp.index:
            index.append(value)
        # Creamos un diccionario en el que se mapean los valores de la lista anterior
        # dicc = dict(zip(sorted(index), range(0,len(index))))
        dicc = dict(zip(range(0,len(index)), sorted(index)))
        # Si en la columna hay algun NaN, añadimos un nuevo elemento clave-valor en el diccionario y lo sustituimos por el elemento
        if (series.isnull().any()):
            count = len(index)
            dicc[count] = 'No_Aplica'
            series = series.map(lambda x : count if str(x) == str(np.NaN) else x)
        #mapped = series.map(lambda x: dicc[x])
        mapped = series.map(lambda x: toDict(x,dicc) if count != x else x)
        data[header] = mapped
        dictionary[header] = dicc
    return data, dictionary

# Llamada a la función anterior
categorical_df, di = codify(complet_dataframe[categorical_headers], categorical_headers)

# Parseamos los datos del diccionario a un json
di = json.dumps(di)


# **************************************************************
# Descomentar la siguiente linea para generar fichero categoricals
writeHeader(categorical, categorical_df)

def toFile(data, file):
    filename = open(file, 'w')
    filename.write(data)
    filename.close()

# **************************************************************
# Descomentar la siguiente linea para generar fichero dictionary
toFile(di, dictionary)





